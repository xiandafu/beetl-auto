package beetl.auto.function;


import beetl.auto.annots.FunctionDef;
import org.beetl.core.Context;
import org.beetl.core.Function;

import java.time.LocalDate;

@FunctionDef("copyright")
public class ShowCopyrightFunction implements Function {

    @Override
    public Object call(Object[] paras, Context ctx) {

        return "Copyright &copy; "+LocalDate.now().toString();
    }
}
