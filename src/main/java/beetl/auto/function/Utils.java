package beetl.auto.function;


import beetl.auto.annots.FunctionPackageDef;

@FunctionPackageDef("myutil")
public class Utils {
    public String a(){
        return "a";
    }
    public String b(){
        return "b";
    }
}
