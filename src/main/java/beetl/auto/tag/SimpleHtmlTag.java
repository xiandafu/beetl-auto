package beetl.auto.tag;

import beetl.auto.annots.TagDef;
import org.beetl.core.tag.GeneralVarTagBinding;

import java.io.IOException;
import java.util.Map;

@TagDef("tag")
public class SimpleHtmlTag  extends GeneralVarTagBinding {
    @Override
    public void render(){
        String tagName = (String) this.args[0];
        Map attrs = (Map) args[1];
        String value = (String) attrs.get("attr");
        //Map allColsDefine = (map)attrs.get("$cols");
        try{
            this.ctx.byteWriter.writeString(value);
        }catch (IOException e){

        }
    }
}
