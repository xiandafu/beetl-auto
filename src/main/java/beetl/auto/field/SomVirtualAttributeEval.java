package beetl.auto.field;

import beetl.auto.annots.VirtualDef;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.beetl.core.Context;
import org.beetl.core.VirtualAttributeEval;


@VirtualDef
public class SomVirtualAttributeEval implements VirtualAttributeEval {

    Gson gson = new GsonBuilder().setPrettyPrinting().disableInnerClassSerialization().disableHtmlEscaping()
            .enableComplexMapKeySerialization()
            .setLenient()
            .create();

    @Override
    public boolean isSupport(Class c, String attributeName) {
        return true;
    }

    @Override
    public Object eval(Object o, String attributeName, Context ctx) {
        if(attributeName.equals("json"))
            return gson.toJson(o);
        return "";
    }
}
