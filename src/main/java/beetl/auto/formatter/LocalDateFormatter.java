package beetl.auto.formatter;

import beetl.auto.annots.FormatDef;
import com.google.common.base.Strings;
import org.beetl.core.Format;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;


@FormatDef(value={LocalDate.class, LocalDateTime.class},name = "datef")
public class LocalDateFormatter implements Format {
    String defaultPattern = "y-M-d";
    @Override
    public Object format(Object data, String pattern) {
        String ptn = Strings.isNullOrEmpty(pattern)?defaultPattern:pattern;
        if(!(data instanceof TemporalAccessor))
            throw new UnsupportedOperationException("supported type must be java.time.temporal.TemporalAccessor");

        return DateTimeFormatter.ofPattern(ptn).format((TemporalAccessor)data);

    }
}
