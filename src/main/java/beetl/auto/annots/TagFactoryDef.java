package beetl.auto.annots;


import java.lang.annotation.*;

/**
 * 注册一个 tagFactory
 * */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TagFactoryDef {
    String value();
}
