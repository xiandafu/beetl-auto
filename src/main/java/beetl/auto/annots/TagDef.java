package beetl.auto.annots;

import java.lang.annotation.*;

/**
 * 注册一个 tag
 * */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TagDef {
    /**
     * name of tag <#input></#input>
     * */
    String value();
    boolean html() default false;
}
