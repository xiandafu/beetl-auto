package beetl.auto.annots;

import java.lang.annotation.*;

/**
 * 注册一个函数
 * */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FunctionDef {
    String value();
}
