package beetl.auto.annots;

import java.lang.annotation.*;


/**
 * 注册一个类中的所有public方法为函数
 * */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FunctionPackageDef {
    String value();
}
