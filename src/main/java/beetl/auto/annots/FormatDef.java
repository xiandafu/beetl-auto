package beetl.auto.annots;

import java.lang.annotation.*;
/**
 * 注册一个格式化函数
 * */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FormatDef {
    /**
     * 如果不为空则注册为格式化函数
     * */
    String name() default "";
    /**
     * 如果有值则注册为类默认格式化函数
     * */
    Class<?>[] value() default {};
}
