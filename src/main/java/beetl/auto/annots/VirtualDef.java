package beetl.auto.annots;


import java.lang.annotation.*;

/**
 * 注册虚拟属性
 * */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface VirtualDef {
    /**
     * 默认注册所有虚拟属性,如果value指定则注册指定类的虚拟属性
     * */
    Class<?> value() default Void.class;
}
